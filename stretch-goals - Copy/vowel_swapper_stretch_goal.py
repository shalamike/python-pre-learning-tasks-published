def vowel_swapper(string):
    strList = list(string)
    chrList = [["a", "A"],["e", "E"], ["i", "I"], ["o"], ["O"], ["u", "U"]]
    swapList = ["4", "3", "!", "ooo", "OOO", "\/"]
    for y in range(len(strList)):
        counter = 0
        for i in range(len(chrList)):
            for j in range(len(chrList[i])):
                if chrList[i][j] == strList[y]:
                    counter = counter + 1
                    if counter == 2:
                        strList[y] = swapList[i]
                        break 
                        
    reformedString = ''.join(strList)
    return reformedString
                
            
        
        
    
    # ==============
    # Your code here

    # ==============

print(vowel_swapper("aAa eEe iIi oOo uUu")) # Should print "a/\a e3e i!i o000o u\/u" to the console
print(vowel_swapper("Hello World")) # Should print "Hello Wooorld" to the console 
print(vowel_swapper("Everything's Available")) # Should print "Ev3rything's Av/\!lable" to the console
