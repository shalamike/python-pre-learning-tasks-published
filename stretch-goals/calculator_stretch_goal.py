def calculator(a, b, operator):
    if operator == "+":
        return binaryConverter(a + b)
    elif operator == "-":
        return binaryConverter(a - b)
    elif operator == "*":
        return binaryConverter(a * b)
    elif operator == "/":
        return binaryConverter(a / b)
    else:
        return "invalid operator"
    
def binaryConverter(number):
    reversedBinary = ""
    while number != 0:
        if number % 2 != 0:
            reversedBinary += "1"
        else:
            reversedBinary += "0"
        number = number // 2
    return reversedBinary[::-1]
        
        


print(calculator(2, 4, "+")) # Should print 110 to the console
print(calculator(10, 3, "-")) # Should print 111 to the console
print(calculator(4, 7, "*")) # Should output 11100 to the console
print(calculator(100, 2, "/")) # Should print 110010 to the console
