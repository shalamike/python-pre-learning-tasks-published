def factors(number):
    factorList = []
    for i in range(2, number-1):
        if number % i == 0:
            factorList.append(i)
    return factorList

print(factors(15)) # Should print [3, 5] to the console
print(factors(12)) # Should print [2, 3, 4, 6] to the console
print(factors(13)) # Should print â€œ[]â€� (an empty list) to the console
